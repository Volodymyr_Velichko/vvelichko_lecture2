import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

/*
Скрипт А. Логин в Админ панель
        1. Открыть страницу Админ панели
        2. Ввести логин, пароль и нажать кнопку Логин.
        3. После входа в систему нажать на пиктограмме пользователя в верхнем
        правом углу и выбрать опцию «Выход.»
*/

public class ScriptB {
    public static void main(String[] args) {
        WebDriver driver = getDriver();
        String titleOriginal, titleRefreshed;
        driver.manage().window().maximize();

        //Открыть страницу Админ панели
        driver.get("http://prestashop-automation.qatestlab.com.ua/admin147ajyvk0/");
        //Ввести логин, пароль и нажать кнопку Логин.
        //Логин: webinar.test@gmail.com
        WebElement login_form = driver.findElement(By.id("email"));
        login_form.sendKeys("webinar.test@gmail.com");
        //Пароль: Xcg7299bnSmMuRLp9ITw
        WebElement password = driver.findElement(By.id("passwd"));
        password.sendKeys("Xcg7299bnSmMuRLp9ITw");

        //waiting
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        WebElement submit = driver.findElement(By.name("submitLogin"));
        submit.click();

        //waiting
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //Dashboard ************************************************************************
        WebElement menuDashboard = driver.findElement(By.id("tab-AdminDashboard"));
        menuDashboard.click();
        titleOriginal = driver.getTitle();
        System.out.println("Page title is " + titleOriginal);

        driver.navigate().refresh();

        //waiting
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        titleRefreshed = driver.getTitle();
        System.out.println("Page title is " + titleRefreshed);

        //compare titles before and after refreshing
        if(titleOriginal.toString().equals(titleRefreshed.toString())) System.out.println("Page titles are the same, before and after refresh");
        else System.out.println("Page titles are DIFFERENT !");
        //Окончание проверки ***********************************************************************

        //Заказы ***********************************************************************
        WebElement menuZakazi = driver.findElement(By.id("subtab-AdminParentOrders"));
        menuZakazi.click();
        titleOriginal = driver.getTitle();
        System.out.println("Page title is " + titleOriginal);

        driver.navigate().refresh();

        //waiting
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        titleRefreshed = driver.getTitle();
        System.out.println("Page title is " + titleRefreshed);

        //compare titles before and after refreshing
        if(titleOriginal.toString().equals(titleRefreshed.toString())) System.out.println("Page titles are the same, before and after refresh");
        else System.out.println("Page titles are DIFFERENT !");
        //Окончание проверки **********************************************************************

        //Каталог ************************************************************************
        WebElement menuKatalog = driver.findElement(By.id("subtab-AdminCatalog"));
        menuKatalog.click();
        titleOriginal = driver.getTitle();
        System.out.println("Page title is " + titleOriginal);

        driver.navigate().refresh();

        //waiting
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        titleRefreshed = driver.getTitle();
        System.out.println("Page title is " + titleRefreshed);

        //compare titles before and after refreshing
        if(titleOriginal.toString().equals(titleRefreshed.toString())) System.out.println("Page titles are the same, before and after refresh");
        else System.out.println("Page titles are DIFFERENT !");

        driver.navigate().back();

        //Окончание проверки ***********************************************************************

        //Клиенты ************************************************************************
        WebElement menuKlienti = driver.findElement(By.id("subtab-AdminParentCustomer"));
        menuKlienti.click();
        titleOriginal = driver.getTitle();
        System.out.println("Page title is " + titleOriginal);

        driver.navigate().refresh();

        //waiting
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        titleRefreshed = driver.getTitle();
        System.out.println("Page title is " + titleRefreshed);

        //compare titles before and after refreshing
        if(titleOriginal.toString().equals(titleRefreshed.toString())) System.out.println("Page titles are the same, before and after refresh");
        else System.out.println("Page titles are DIFFERENT !");
        //waiting
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //Окончание проверки ***********************************************************************

        //Служба поддержки ************************************************************************
        WebElement menuSlujbaPodderjki = driver.findElement(By.id("subtab-AdminParentCustomerThreads"));
        menuSlujbaPodderjki.click();
        titleOriginal = driver.getTitle();
        System.out.println("Page title is " + titleOriginal);

        driver.navigate().refresh();

        //waiting
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        titleRefreshed = driver.getTitle();
        System.out.println("Page title is " + titleRefreshed);

        //compare titles before and after refreshing
        if(titleOriginal.toString().equals(titleRefreshed.toString())) System.out.println("Page titles are the same, before and after refresh");
        else System.out.println("Page titles are DIFFERENT !");
        //Окончание проверки ***********************************************************************

        //Статистика ************************************************************************
        WebElement menuStatistika = driver.findElement(By.id("subtab-AdminStats"));
        menuStatistika.click();
        titleOriginal = driver.getTitle();
        System.out.println("Page title is " + titleOriginal);

        driver.navigate().refresh();

        //waiting
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        titleRefreshed = driver.getTitle();
        System.out.println("Page title is " + titleRefreshed);

        //compare titles before and after refreshing
        if(titleOriginal.toString().equals(titleRefreshed.toString())) System.out.println("Page titles are the same, before and after refresh");
        else System.out.println("Page titles are DIFFERENT !");
        //Окончание проверки ***********************************************************************

        //Modules ************************************************************************
        WebElement menuModules = driver.findElement(By.id("subtab-AdminParentModulesSf"));
        menuModules.click();
        titleOriginal = driver.getTitle();
        System.out.println("Page title is " + titleOriginal);

        driver.navigate().refresh();

        //waiting
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        titleRefreshed = driver.getTitle();
        System.out.println("Page title is " + titleRefreshed);

        //compare titles before and after refreshing
        if(titleOriginal.toString().equals(titleRefreshed.toString())) System.out.println("Page titles are the same, before and after refresh");
        else System.out.println("Page titles are DIFFERENT !");

        driver.navigate().back();
        //Окончание проверки ***********************************************************************

        //Design ************************************************************************
        WebElement menuDesign = driver.findElement(By.id("subtab-AdminParentThemes"));
        menuDesign.click();
        titleOriginal = driver.getTitle();
        System.out.println("Page title is " + titleOriginal);

        driver.navigate().refresh();

        //waiting
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        titleRefreshed = driver.getTitle();
        System.out.println("Page title is " + titleRefreshed);

        //compare titles before and after refreshing
        if(titleOriginal.toString().equals(titleRefreshed.toString())) System.out.println("Page titles are the same, before and after refresh");
        else System.out.println("Page titles are DIFFERENT !");
        //Окончание проверки ***********************************************************************

        //Доставка ************************************************************************
        WebElement menuDostavka = driver.findElement(By.id("subtab-AdminParentShipping"));
        menuDostavka.click();
        titleOriginal = driver.getTitle();
        System.out.println("Page title is " + titleOriginal);

        driver.navigate().refresh();

        //waiting
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        titleRefreshed = driver.getTitle();
        System.out.println("Page title is " + titleRefreshed);

        //compare titles before and after refreshing
        if(titleOriginal.toString().equals(titleRefreshed.toString())) System.out.println("Page titles are the same, before and after refresh");
        else System.out.println("Page titles are DIFFERENT !");
        //Окончание проверки ***********************************************************************

        //Способ оплаты ************************************************************************
        WebElement menuSposobOplati = driver.findElement(By.id("subtab-AdminParentPayment"));
        menuSposobOplati.click();
        titleOriginal = driver.getTitle();
        System.out.println("Page title is " + titleOriginal);

        driver.navigate().refresh();

        //waiting
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        titleRefreshed = driver.getTitle();
        System.out.println("Page title is " + titleRefreshed);

        //compare titles before and after refreshing
        if(titleOriginal.toString().equals(titleRefreshed.toString())) System.out.println("Page titles are the same, before and after refresh");
        else System.out.println("Page titles are DIFFERENT !");
        //Окончание проверки ***********************************************************************

        //International ************************************************************************
        WebElement menuInternational = driver.findElement(By.id("subtab-AdminInternational"));
        menuInternational.click();
        titleOriginal = driver.getTitle();
        System.out.println("Page title is " + titleOriginal);

        driver.navigate().refresh();

        //waiting
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        titleRefreshed = driver.getTitle();
        System.out.println("Page title is " + titleRefreshed);

        //compare titles before and after refreshing
        if(titleOriginal.toString().equals(titleRefreshed.toString())) System.out.println("Page titles are the same, before and after refresh");
        else System.out.println("Page titles are DIFFERENT !");
        //Окончание проверки ***********************************************************************

        //ShopParameters ************************************************************************
        WebElement menuShopParameters = driver.findElement(By.id("subtab-ShopParameters"));
        menuShopParameters.click();
        titleOriginal = driver.getTitle();
        System.out.println("Page title is " + titleOriginal);

        driver.navigate().refresh();

        //waiting
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        titleRefreshed = driver.getTitle();
        System.out.println("Page title is " + titleRefreshed);

        //compare titles before and after refreshing
        if(titleOriginal.toString().equals(titleRefreshed.toString())) System.out.println("Page titles are the same, before and after refresh");
        else System.out.println("Page titles are DIFFERENT !");
        //Окончание проверки ***********************************************************************

        //Konfigurazija ************************************************************************
        WebElement menuKonfigurazija = driver.findElement(By.id("subtab-AdminAdvancedParameters"));
        menuKonfigurazija.click();
        titleOriginal = driver.getTitle();
        System.out.println("Page title is " + titleOriginal);

        driver.navigate().refresh();

        //waiting
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        titleRefreshed = driver.getTitle();
        System.out.println("Page title is " + titleRefreshed);

        //compare titles before and after refreshing
        if(titleOriginal.toString().equals(titleRefreshed.toString())) System.out.println("Page titles are the same, before and after refresh");
        else System.out.println("Page titles are DIFFERENT !");
        //Окончание проверки ***********************************************************************


        //После входа в систему нажать на пиктограмме пользователя в верхнем правом углу и выбрать опцию «Выход.»
//
//        WebElement profile_button = driver.findElement(By.id("employee_infos"));
//        profile_button.click();
//
//        try {
//            Thread.sleep(3000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//
//        WebElement logout = driver.findElement(By.id("header_logout"));
//        logout.click();

        driver.close();
    }

    public static WebDriver getDriver(){
        System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "/resources/chromedriver.exe");
        return new ChromeDriver();
    }
}
