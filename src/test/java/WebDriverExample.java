import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class WebDriverExample {
    public static void main(String[] args) {
        WebDriver driver = getDriver();
        driver.manage().window().maximize();
        driver.get("https://www.bing.com/");
        WebElement searchInput = driver.findElement(By.id("sb_form_q"));
        searchInput.sendKeys("Selenium");

        WebElement searchButton = driver.findElement(By.name("go"));
        searchButton.click();

        System.out.println("Page title is " + driver.getTitle());

        driver.close();
    }

        public static WebDriver getDriver(){

        System.setProperty("webdriver.chrome.driver", "C:\\Users\\vvelichko\\IdeaProjects\\adsf\\resources\\chromedriver.exe");
            //System.setProperty("wedriver.chrome.driver", System.getProperty("user.dir") + "\\resources\\chromedriver.exe");

        return new ChromeDriver();
        }
}
